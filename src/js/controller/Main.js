import $ from 'jquery';

import Controller from '../game/Controller';

export default class Main {

    constructor(api) {
        this._api = api;
        this._paddleId = 0;
    }

    run() {
        $('button').on('click', e => {
            e.preventDefault();
            this._paddleId = $(e.currentTarget).data('id');
            $('body').css('border-color', $(e.currentTarget).data('color'));
            $('.exit').css({
                background: $(e.currentTarget).data('color')
            });
            $('.select-player').hide();
            $('.controls').show();

            $('body').on('mousedown touchstart', function () {
                $(this).addClass('pressed');
            });

            $('body').on('mouseup touchend', function () {
                $(this).removeClass('pressed');
            });

            const controller = new Controller();
            controller.registerEventListener(offset => {
                this._api.emit('paddle-move', {
                    paddleId: this._paddleId,
                    y: offset
                });
            });
        });

        $('.exit').on('mousedown touchstart', function (e) {
            e.stopPropagation();
            $(this).addClass('pressed');
        });
        $('.exit').on('mouseup touchend', function (e) {
            e.stopPropagation();
            $(this).removeClass('pressed');
        });
        $('.exit').on('click', e => {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('really?')) {
                this._api.emit('controller-exit');
            }
        });

        this._api.on('screen-exit', () => {
            // TODO : use socket-games-api
            top.JSCONST.stopGame();
        });
    }

}
