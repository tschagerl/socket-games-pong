export default {
    // constants
    PADDLE_ID_LEFT: 0,
    PADDLE_ID_RIGHT: 1,

    // board settings
    canvasWidth: 500,
    canvasHeight: 500,
    useFullscreen: false,

    // game settings
    baseSpeed: 400,            // pixel per second
    hitAcceleration: 1.1,
    pongRadius: 30,            // in pixel
    paddleWidth: 30,           // in pixel
    paddleHeight: 200          // in pixel
};
