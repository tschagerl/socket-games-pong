import $ from 'jquery';

export default class Controller {

    registerEventListener(callback) {
        this._callback = callback;
        $(document).off('touchstart touchmove touchend').on('touchstart touchmove touchend', e => this.onTouchEvent(e));
        $(document).off('mousemove').on('mousemove', e => this.onMouseMove(e));
    }

    onTouchEvent(e) {
        e.preventDefault();
        const touch = e.originalEvent.touches[0];
        const offset = Math.min(1, Math.max(touch.pageY / $(window).height(), 0));
        this._callback(offset);
    }

    onMouseMove(e) {
        const offset = Math.min(1, Math.max(e.pageY / $(window).height(), 0));
        this._callback(offset);
    }

}
