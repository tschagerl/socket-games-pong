import {requestAnimationFrame} from './utils';

export default class Engine {
    constructor(drawingContext, leftPaddle, rightPaddle, config) {
        this._drawingContext = drawingContext;
        this._leftPaddle = leftPaddle;
        this._rightPaddle = rightPaddle;
        this._config = config;
        this._running = false;
        this._lastUpdate = 0;
        this._pong = null;
    }

    reset() {
        this._pong = {
            x: 0.5 * this._config.canvasWidth,
            y: 0.5 * this._config.canvasHeight,
            angle: 0,
            speed: this._config.baseSpeed
        };
        this._pong.angle = Math.random() * 60;
        if (Math.random() > 0.5) {
            this._pong.angle += 180;
        }
        if (Math.random() > 0.5) {
            this._pong.angle *= -1;
        }
        this._lastUpdate = window.performance.now();
    }

    start() {
        if (this._running) {
            return;
        }
        this._running = true;

        this.reset();
        requestAnimationFrame(() => this._update());
    }

    stop() {
        this._running = false;
    }

    _update() {
        if (!this._running) {
            return;
        }
        const now = window.performance.now();
        const deltaTime = now - this._lastUpdate;
        this._lastUpdate = now;
        this._updatePong(deltaTime);
        this._draw(deltaTime);
        requestAnimationFrame(() => this._update());
    }

    _updatePong(deltaTime) {
        let deltaX = Math.cos(this._pong.angle * Math.PI / 180) * this._pong.speed * deltaTime / 1000,
            deltaY = Math.sin(this._pong.angle * Math.PI / 180) * this._pong.speed * deltaTime / 1000,
            reflectVertical,
            reflectHorizontal,
            paddleTop;

        // CHECK NORTH/SOUTH COLLISION
        if (this._pong.y + deltaY < this._config.pongRadius) {
            reflectVertical = true;
        } else if (this._pong.y + deltaY > this._config.canvasHeight - this._config.pongRadius) {
            reflectVertical = true;
        }
        if (reflectVertical) {
            deltaY *= -1;
        }

        // CHECK EAST/WEST COLLISION
        if (this._pong.x + deltaX > this._config.canvasWidth - this._config.pongRadius - this._config.paddleWidth) {
            paddleTop = Math.max(0, this._rightPaddle.y * this._config.canvasHeight - this._config.paddleHeight / 2);
            paddleTop = Math.min(paddleTop, this._config.canvasHeight - this._config.paddleHeight);
            if (this._pong.y + deltaY < paddleTop
                ||
                this._pong.y + deltaY > paddleTop + this._config.paddleHeight) {
                ++this._leftPaddle.points;
                this.reset();
                return;
            }
            reflectHorizontal = true;
        } else if (this._pong.x + deltaX < this._config.pongRadius + this._config.paddleWidth) {
            paddleTop = Math.max(0, this._leftPaddle.y * this._config.canvasHeight - this._config.paddleHeight / 2);
            paddleTop = Math.min(paddleTop, this._config.canvasHeight - this._config.paddleHeight);
            if (this._pong.y + deltaY < paddleTop
                ||
                this._pong.y + deltaY > paddleTop + this._config.paddleHeight) {
                ++this._rightPaddle.points;
                this.reset();
                return;
            }
            reflectHorizontal = true;
        }
        if (reflectHorizontal) {
            deltaX *= -1;
            this._pong.speed *= this._config.hitAcceleration;
        }

        // correct the flying direction
        if (reflectHorizontal || reflectVertical) {
            this._pong.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
        }

        this._pong.x += deltaX;
        this._pong.y += deltaY;
    }

    _draw() {
        let paddleTop;

        // clear board
        this._drawingContext.fillStyle = 'black';
        this._drawingContext.fillRect(0, 0, this._config.canvasWidth, this._config.canvasHeight);

        // draw borders (top, bottom, middle line)

        // draw scores
        this._drawingContext.fillStyle = 'grey';
        this._drawingContext.font = '30px Arial';
        this._drawingContext.fillText(this._leftPaddle.points, this._config.canvasWidth * 0.4, this._config.canvasHeight * 0.1);
        this._drawingContext.fillText(this._rightPaddle.points, this._config.canvasWidth * 0.6, this._config.canvasHeight * 0.1);

        // draw paddles
        paddleTop = Math.max(0, this._leftPaddle.y * this._config.canvasHeight - this._config.paddleHeight / 2);
        paddleTop = Math.min(paddleTop, this._config.canvasHeight - this._config.paddleHeight);
        this._drawingContext.fillStyle = 'red';
        this._drawingContext.fillRect(0, paddleTop, this._config.paddleWidth, this._config.paddleHeight);
        paddleTop = Math.max(0, this._rightPaddle.y * this._config.canvasHeight - this._config.paddleHeight / 2);
        paddleTop = Math.min(paddleTop, this._config.canvasHeight - this._config.paddleHeight);
        this._drawingContext.fillStyle = 'green';
        this._drawingContext.fillRect(this._config.canvasWidth - this._config.paddleWidth, paddleTop, this._config.paddleWidth, this._config.paddleHeight);

        // draw pong
        this._drawingContext.beginPath();
        this._drawingContext.arc(this._pong.x, this._pong.y, this._config.pongRadius, 0, 2 * Math.PI, false);
        this._drawingContext.fillStyle = 'white';
        this._drawingContext.fill();
    }

}
