import Config from './Config';
import Engine from './Engine';
import Paddle from './Paddle';

export default class Game {

    constructor(canvasID, settings) {
        this._config = Object.assign({}, Config, settings);

        this._canvasElement = document.getElementById(canvasID);

        if (this._config.useFullscreen) {
            this._config.canvasWidth = window.innerWidth;
            this._config.canvasHeight = window.innerHeight;
        }

        this._canvasElement.width = this._config.canvasWidth;
        this._canvasElement.height = this._config.canvasHeight;

        if (this._canvasElement.getContext) {
            this._drawingContext = this._canvasElement.getContext('2d');
        } else {
            throw 'No canvas support';
        }

        this._leftPaddle = new Paddle();
        this._rightPaddle = new Paddle();
        this.engine = new Engine(this._drawingContext, this._leftPaddle, this._rightPaddle, this._config);
    }

    updateConfig(settings) {
        Object.keys(settings).forEach(function (key) {
            this._config[key] = settings[key];
        }, this);
        this._canvasElement.width = this._config.canvasWidth;
        this._canvasElement.height = this._config.canvasHeight;
    }

    start() {
        this.engine.start();
    }

    setPaddle(paddleId, y) {
        if (paddleId === Config.PADDLE_ID_LEFT) {
            this._leftPaddle.y = y;
        } else if (paddleId === Config.PADDLE_ID_RIGHT) {
            this._rightPaddle.y = y;
        }
    }

}
