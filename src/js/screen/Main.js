import $ from 'jquery';
import qrcodejs from 'qrcodejs';

import Pong from '../game/Game';

export default class Main {

    constructor(api) {
        this._api = api;
    }

    run() {
        Main._renderScreenId();

        const $content = $('.content').html('<canvas id="screenCanvas">');

        // create game
        const pong = new Pong('screenCanvas', {
            canvasWidth: $content.outerWidth(),
            canvasHeight: $content.outerHeight()
        });
        pong.start();

        // listen to paddle moves
        this._api.on('paddle-move', data => {
            pong.setPaddle(Number(data.paddleId), Number(data.y));
        });

        this._api.on('controller-exit', () => {
            setTimeout(() => {
                this._api.emit('screen-exit');
                // TODO : use socket-games-api
                top.JSCONST.stopGame();
            }, 10);
        });
    }

    static _renderScreenId() {
        // TODO : show big image that explains how to connect smartphone
        // ATTENTION: this method has a duplicate in gamecenter MainView
        const $screenId = $('.controller-link');
        const url = top.JSCONST.gameCenterControllerUrl;
        $screenId.html('<a href="' + url + '" target="_blank">' + url + '</a>');
        new qrcodejs.QRCode($('.qrcode')[0], url);
    }

}
